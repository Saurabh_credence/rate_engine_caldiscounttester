const fs = require('fs');

function calDiscount(offer,trnDET) {
    try{
        let totalOffer = {};
        if(offer.validFor =="RC")
        {
            let frate = calRateDis(offer,trnDET);
            if (frate.status=="unsuccess")
                return frate;
            let totalCharge = calCharges(offer,trnDET);
            if (totalCharge.status=="unsuccess")
                return totalCharge;
            totalOffer = {"offerdetail":{"chargeDetail":totalCharge,"rateDetail":frate}};
        }else if(offer.validFor =="R")
        {
            let frate = calRateDis(offer,trnDET);
            if (frate.status=="unsuccess")
                return frate;
            totalOffer = {"offerdetail":{"rateDetail":frate}};
        }else if(offer.validFor =="C")
        {
            let totalCharge = calCharges(offer,trnDET);
            if (totalCharge.status=="unsuccess")
                return totalCharge;
            totalOffer = {"offerdetail":{"chargeDetail":totalCharge}};
        }else{
            totalOffer = {"status": "unsuccess", "msg": "offer not applicable" ,"applicable":"N"};
        }
        let tolDis = calTotalDiscount(totalOffer.offerdetail,trnDET,offer.validFor);
        totalOffer.offerdetail.transDetail = tolDis;
        totalOffer.status = "success";
        return JSON.stringify(totalOffer);
    }
    catch(err)
    {
        return { "status": "unsuccess", "msg": err.message,"applicable":"N"};
    }
}

function calCharges(offer,trnDET)
{
    try {
        if(offer.chargesDiscount.chargeDiscountType=='F')
        {
            let chargeNG = trnDET.orgCharges - offer.chargesDiscount.chargeDiscount;
            if (chargeNG>=0)
            {
                let gst = chargeNG * (trnDET.GST/100);
                var totalCharge = chargeNG + gst;
            }else
            {
                return {"status": "unsuccess", "msg": "charges should not less then zero"};
            }
        }
        else
        {
            let chargeNG = trnDET.orgCharges - (trnDET.orgCharges * (offer.chargesDiscount.chargeDiscount/100));
            if (chargeNG>=0)
            {
                let gst = chargeNG * (trnDET.GST/100);
                var totalCharge = chargeNG + gst;
            }else
            {
                return {"status": "unsuccess", "msg": "charges should not less then zero"};
            }
        }
        let appliCharge = { "chargeDiscountType": offer.chargesDiscount.chargeDiscountType, "chargesDiscount": offer.chargesDiscount.chargeDiscount,"orgCharge": trnDET.orgCharges,"charges":totalCharge.toFixed(2).toString()};
        return appliCharge;    
    } catch (err) {
        return { "status": "unsuccess", "msg": err.message};
    }
}

function calRateDis(offer,trnDET)
{
    try{
        let ibrate;
        let cardRate;
        let margin;
        if(offer.rateDiscount.rateDiscountOn == "IBR")
        {
            ibrate = getIBRate(trnDET);
            if(offer.rateDiscount.rateDiscountType=="F")
            {
                margin = (offer.applicableRateMargin/100) * trnDET.buySellSign;
                cardRate = parseFloat(ibrate) - margin;
            }
            else if(offer.rateDiscount.rateDiscountType=="P")
            {
                margin = (ibrate*(offer.applicableRateMargin/100)) * trnDET.buySellSign;
                cardRate = parseFloat(ibrate) - margin;
            }
            else
            {
                return {"status":"unsuccess","msg":"rate discount is not applicable"};
            }
        }
        else if(offer.rateDiscount.rateDiscountOn == "C"){
            margin = (offer.applicableRateMargin/100) * trnDET.buySellSign;
            cardRate = parseFloat(trnDET.cardRate) + margin;
        }
        else
        {
            return {"status":"unsuccess","msg":"rate discount is not applicable"};
        }
        if(cardRate<ibrate && trnDET.buySellSign=="-1")
            cardRate = parseFloat(ibrate);
        else if(cardRate>ibrate && trnDET.buySellSign=="1")
            cardRate = parseFloat(ibrate);
        let frate = {
            "rateDiscountType":offer.rateDiscount.rateDiscountType,
            "rateDiscountOn":offer.rateDiscount.rateDiscountOn,
            "rateDiscountOrMargin":offer.rateDiscount.rateDiscountOrMargin,
            "orgRate":trnDET.rate,
            "rate":cardRate.toFixed(5),
            "ibr":trnDET.ibr,
            "cardRate":trnDET.cardRate
        }
        return frate;
    }catch(err)
    {
        return { "status": "unsuccess", "msg": err.message,"applicable":"N"};
    }
}

function calTotalDiscount(offer,trnDET,validFor)
{
    let icyAmt,tolAmt,tolDis;
    let orgChargeGst = parseFloat(trnDET.orgCharges) + (parseFloat(trnDET.orgCharges) * (parseFloat(trnDET.GST)/100));
    if(validFor=="RC")
    {
        icyAmt = parseFloat(trnDET.amount) * parseFloat(offer.rateDetail.rate/trnDET.perUnit);
        tolAmt = icyAmt + parseFloat(offer.chargeDetail.charges);
        tolDis = tolAmt - ((parseFloat(trnDET.amount) * parseFloat(offer.rateDetail.orgRate)) + orgChargeGst);
    }else if(validFor=="R")
    {
        icyAmt = parseFloat(trnDET.amount) * (parseFloat(offer.rateDetail.rate/trnDET.perUnit));
        tolAmt = icyAmt + orgChargeGst;
        tolDis = tolAmt - ((parseFloat(trnDET.amount) * parseFloat(offer.rateDetail.orgRate)) + orgChargeGst);
    }else if(validFor=="C")
    {
        icyAmt = parseFloat(trnDET.amount) * parseFloat(trnDET.cardRate/trnDET.perUnit);
        tolAmt = icyAmt + parseFloat(offer.chargeDetail.charges);
        tolDis = tolAmt - ((trnDET.amount * trnDET.cardRate) + orgChargeGst);
    }
    else
    {
        tolDis = 0;
    }
    let transDET = {
        "transTypeCode":trnDET.transTypeCode,
        "currency":trnDET.currency,
        "fcyAmount":trnDET.amount,
        "lcyAmount":icyAmt.toFixed(2),
        "totalAmount":tolAmt.toFixed(2),
        "totalDiscount":tolDis.toFixed(2)
    }
    return transDET;
}

function getIBRate(trnDET)
{
    return trnDET.ibr;
}

// let trndel = JSON.parse(fs.readFileSync('trndel.json','utf-8'));
// let offer = JSON.parse(fs.readFileSync('selectedOfferCode.json','utf-8'));
// let caloffer = calDiscount(offer,trndel);
// console.log(caloffer);


module.exports = calDiscount;