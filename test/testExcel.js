const { assert } =require('chai');
const totalDis = require('../tester');
let data = totalDis();

describe('Testing the Total Discount function',function(){  
    for(let i=0;i<data.Expected.length;i++){
        describe(`Testing : Test case no.${i + 1}`,function(){
            it('Actual JSON output should match with Expected JSON output',function(){
                assert.deepEqual(data.Actual[i],data.Expected[i]);
            })
        });
    }
});