const xlsx = require('xlsx');
const caldist = require('./rate');

function totalDiscountTester() {
    try{
        const workbook = xlsx.readFile('Rate Engine Test Cases.xlsx');
        const excel_sheet = workbook.Sheets['CD-TestFamily-CalculateDiscount'];

        //Array objects
        let expected = [], actual = [];

        //JSON objects
        let transJson, offerJson, expectedOutJson, actualOutJson;

        //Count total number of rows
        let rowCount = 0;
        for(let x of Object.keys(excel_sheet)){
            if (x[0] == "I"){
                rowCount += 1;
            }
        }

        //Reading data from excel file (row wise)
        let addr;
        for(let i=3; i<=rowCount; i++){

            //getting transaction details JSON (column I)
            addr = "I"+i;
            transJson = (excel_sheet[addr].v).replace('trnDetail:','');
            transJson = (eval('['+transJson+']'))[0];

            //getting selected offer code JSON (column J)
            addr = "J"+i;
            offerJson = (excel_sheet[addr].v).replace('selectedOffer:','');
            offerJson = (eval('['+offerJson+']'))[0];

            //getting expected Output Json (last column S of row)
            addr = "S"+i;
            expectedOutJson = (excel_sheet[addr].v);
            expectedOutJson = (eval('['+expectedOutJson+']'))[0];

            //Calling external rate function
            actualOutJson = JSON.parse(caldist((offerJson),(transJson)));

            expected.push(expectedOutJson); 
            actual.push(actualOutJson);
        }
        return {"Expected":expected,"Actual":actual};
    }
    catch(err){
        console.log(err);
    }
};

module.exports = totalDiscountTester;






